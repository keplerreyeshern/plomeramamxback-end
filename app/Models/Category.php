<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "categories";

    protected $translatable = ['name', 'slug'];

    protected $fillable = [
        'name',
    ];

    public function children() {
        return $this->hasMany(Category::class, "parent_id", "id");
    }

    public function parent() {
        return $this->belongsTo(Category::class, "parent_id", "id");
    }

    public function recursiveChildren() {
        return $this->children()->with("recursiveChildren");
    }
}
