<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\ContactEmail;
use App\Models\Accountant;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\Message;
use App\Models\News;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::where('active', true)->get();
        $galleries = Gallery::where('active', true)->where('type', 'news')->get();
        $result = [
            'news' => $news,
            'galleries' => $galleries
        ];
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $news = News::where('slug', $slug)->first();
        $gallery = Gallery::where('reference', $news->id)->where('type', 'news')->get();
        $result = [
            'news' => $news,
            'gallery' => $gallery
        ];
        return $result;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {
        $for = "gvenancio@plomeramamx.com.mx";
//        $for = "keplerreyeshern@gmail.com";

        $data = new \stdClass();
        $data->name = $request['name'];
        $data->telephone = $request['telephone'];
        $data->email = $request['email'];
        $data->message = $request['message'];

        Mail::to($for)->send(new ContactEmail($data));
        $message = new Message();
        $message->name = $request['name'];
        $message->email = $request['email'];
        $message->telephone = $request['telephone'];
        $message->message = $request['message'];
        $message->save();
        return $message;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function click($type)
    {
        $accountant = new Accountant();
        $accountant->month = date("m");
        $accountant->type = $type;
        $accountant->save();
        return $accountant;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function release($release)
    {
        $news = News::where('slug', $release)->first();
        $gallery = Gallery::where('reference', $news->id)->where('type', 'news')->first();
        $images = Image::where('gallery_id', $gallery->id)->get();
        $result = [
            'news' => $news,
            'gallery' => $gallery,
            'images' => $images
        ];
        return $result;
    }
}
