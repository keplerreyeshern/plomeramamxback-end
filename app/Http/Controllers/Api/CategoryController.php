<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();

        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $category = Category::findOrFail($id);

        return $category;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => $request['language']]);
        $category = new Category();
        $category->name = $request['name'];
        $category->slug = Str::slug($request['name']);
        if($request['parent_id']){
            $category->parent_id = $request['parent_id'];
        }
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/categories/' . $name,  \File::get($file));
            $category->image = '/storage/images/categories/' . $name;
        }
        $category->save();
        if ($request['fileslength'] != '0' ){
            $gallery = new Gallery();
            $gallery->name = $request['name'];
            $gallery->slug = Str::slug($request['name']);
            $gallery->type = 'categories';
            $gallery->reference = $category->id;
            $gallery->save();
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/categories/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/categories/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $category;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        $gallery = Gallery::where('reference', $category->id)->where('type', 'categories')->first();
        if ($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        } else {
            $images = [];
        }

        $result = [
            'category' => $category,
            'images' => $images,
            'gallery' => $gallery,
        ];

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        if ($category->active){
            $category->active = false;
        } else {
            $category->active = true;
        }
        $category->save();

        return $category;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => $request['language']]);
        $category = Category::findOrFail($id);
        $category->name = $request['name'];
        $category->slug = Str::slug($request['name']);
        if($request['parent_id']){
            $category->parent_id = $request['parent_id'];
        }
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/categories/' . $name,  \File::get($file));
            $category->image = '/storage/images/categories/' . $name;
        }
        $category->save();
        if ($request['fileslength'] != '0' ){
            if ($request['gallery']){
                $gallery = Gallery::findOrFail($request['gallery']);
            } else {
                $gallery = new Gallery();
                $gallery->name = $request['name'];
                $gallery->slug = Str::slug($request['name']);
                $gallery->type = 'categories';
                $gallery->reference = $category->id;
                $gallery->save();
            }

            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/categories/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/categories/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return $category;
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function children($id)
    {
        $categories = Category::where('parent_id', $id)->get();


        return $categories;
    }
}
