<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::where('type', 'news')->get();

        return $news;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => $request['language']]);
        $news = new News();
        $news->title = $request['title'];
        $news->slug = Str::slug($request['title']);
        $news->content = $request['content'];
        $news->intro = $request['intro'];
        $news->type = 'news';
        $news->date = date_create('now');
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/news/' . $name,  \File::get($file));
            $news->image = '/storage/images/news/' . $name;
        }
        $news->save();
        if ($request['fileslength'] != '0' ){
            $gallery = new Gallery();
            $gallery->name = $request['title'];
            $gallery->slug = Str::slug($request['title']);
            $gallery->type = 'news';
            $gallery->reference = $news->id;
            $gallery->save();
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/news/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/news/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $news;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::findOrFail($id);
        $gallery = Gallery::where('type', 'news')->where('reference', $news->id)->first();
        if($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        } else {
            $images = [];
        }
        $result = [
            'news' => $news,
            'gallery' => $gallery,
            'images' => $images,
        ];
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        if($news->active){
            $news->active = false;
        } else {
            $news->active = true;
        }

        $news->save();
        return $news;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => $request['language']]);
        $news = News::findOrFail($id);
        $news->title = $request['title'];
        $news->slug = Str::slug($request['title']);
        $news->content = $request['content'];
        $news->intro = $request['intro'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/news/' . $name,  \File::get($file));
            $news->image = '/storage/images/news/' . $name;
        }
        $news->save();
        if ($request['fileslength'] != '0' ){
            $gallery = new Gallery();
            $gallery->name = $request['title'];
            $gallery->slug = Str::slug($request['title']);
            $gallery->type = 'news';
            $gallery->reference = $news->id;
            $gallery->save();
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/news/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/news/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $news;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();

        return $news;
    }
}
